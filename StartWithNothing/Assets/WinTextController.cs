﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinTextController : MonoBehaviour
{
    public static WinTextController instance;

    CanvasGroup canvasGroup;

    [SerializeField] bool visible = false;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;

        canvasGroup = GetComponent<CanvasGroup>();
    }

    // Update is called once per frame
    void Update()
    {
        if (visible) canvasGroup.alpha = Mathf.Lerp(canvasGroup.alpha, 1f, 0.5f);
        else canvasGroup.alpha = Mathf.Lerp(canvasGroup.alpha, 0f, 0.5f);

        if (Mathf.Abs(canvasGroup.alpha - ((visible) ? 1f : 0f)) < 0.01f) canvasGroup.alpha = (visible) ? 1f : 0f;
    }

    public static void SetVisible(bool visible)
    {
        instance.visible = visible;
    }
}
