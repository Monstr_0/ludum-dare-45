﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowArrowController : MonoBehaviour
{
    static Queue<BowArrowController> bowArrowControllers;
    static int maxArrows = 100;

    new Rigidbody rigidbody;
    BoxCollider boxCollider;

    public int damage = 1;

    bool hit = false;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        boxCollider = GetComponent<BoxCollider>();

        if (bowArrowControllers == null) bowArrowControllers = new Queue<BowArrowController>();
        bowArrowControllers.Enqueue(this);
        if (bowArrowControllers.Count > maxArrows)
        {
            BowArrowController bowArrowController = bowArrowControllers.Dequeue();
            Destroy(bowArrowController.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (hit == false) transform.LookAt(transform.position + rigidbody.velocity);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (hit == false)
        {
            if (collision.collider.CompareTag("Enemy") || collision.collider.CompareTag("EnemyBoss"))
            {
                EnemyController enemyController = collision.collider.GetComponent<EnemyController>();
                BossSignController bossSignController = collision.collider.GetComponent<BossSignController>();
                if (enemyController != null)
                {
                    enemyController.TakeDamage(damage);
                    enemyController.Knockback(transform.position, 3f);
                }
                else if (bossSignController != null)
                {
                    bossSignController.Activate(transform.position);
                    Debug.Log("Activate the Boss Sign!");
                }
            }

            rigidbody.constraints = RigidbodyConstraints.FreezeAll;
            boxCollider.enabled = false;
            hit = true;

            transform.SetParent(collision.collider.transform, true);
        }
    }
}
