﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCreator : MonoBehaviour
{
    public bool generating = false;

    public static MapCreator instance;

    public class Cell
    {
        public bool NorthWall = true;
        public bool SouthWall = true;
        public bool WestWall = true;
        public bool EastWall = true;
        public string id;
        public Bounds Bounds;
        public Dictionary<string, Cell> Cells;
        public int Column;
        public int Row;
        public string NeighborNorthID;
        public string NeighborSouthID;
        public string NeighborEastID;
        public string NeighborWestID;
        public bool Visited = false;
        public Stack<Cell> Stack;

        public Cell(Vector2 location, Vector2 size, ref Dictionary<string, Cell> cellList, int r, int c, int maxR, int maxC)
        {
            this.Bounds = new Bounds(location, size);
            this.Column = c;
            this.Row = r;
            this.id = "c" + c + "r" + r;
            int rowNort = r - 1;
            int rowSout = r + 1;
            int colEast = c + 1;
            int colWest = c - 1;
            NeighborNorthID = "c" + c + "r" + rowNort;
            NeighborSouthID = "c" + c + "r" + rowSout;
            NeighborEastID = "c" + colEast + "r" + r;
            NeighborWestID = "c" + colWest + "r" + r;
            if (rowNort < 0) NeighborNorthID = "none";
            if (rowSout > maxR) NeighborSouthID = "none";
            if (colEast > maxC) NeighborEastID = "none";
            if (colWest < 0) NeighborWestID = "none";
            this.Cells = cellList;
            this.Cells.Add(this.id, this);
        }
        public Cell getNeighbor()
        {
            List<Cell> c = new List<Cell>();
            if (!(NeighborNorthID == "none") && Cells[NeighborNorthID].Visited == false) c.Add(Cells[NeighborNorthID]);
            if (!(NeighborSouthID == "none") && Cells[NeighborSouthID].Visited == false) c.Add(Cells[NeighborSouthID]);
            if (!(NeighborEastID == "none") && Cells[NeighborEastID].Visited == false) c.Add(Cells[NeighborEastID]);
            if (!(NeighborWestID == "none") && Cells[NeighborWestID].Visited == false) c.Add(Cells[NeighborWestID]);
            int max = c.Count;
            Cell currentCell = null;
            if (c.Count > 0)
            {
                Random rng = new Random();
                int index = Random.Range(0, c.Count);
                currentCell = c[index];
            }
            return currentCell;
        }
        public Cell Dig(ref Stack<Cell> stack)
        {
            this.Stack = stack;
            Cell nextCell = getNeighbor();
            if ((nextCell != null))
            {
                stack.Push(nextCell);
                if (nextCell.id == this.NeighborNorthID)
                {
                    this.NorthWall = false;
                    nextCell.SouthWall = false;
                }
                else if (nextCell.id == this.NeighborSouthID)
                {
                    this.SouthWall = false;
                    nextCell.NorthWall = false;
                }
                else if (nextCell.id == this.NeighborEastID)
                {
                    this.EastWall = false;
                    nextCell.WestWall = false;
                }
                else if (nextCell.id == this.NeighborWestID)
                {
                    this.WestWall = false;
                    nextCell.EastWall = false;
                }
            }
            else if (!(stack.Count == 0))
            {
                nextCell = stack.Pop();
            }
            else
            {
                return null;
            }
            return nextCell;
        }
    }

    Vector3 playerSpawnPosition = Vector3.zero;

    Dictionary<string, Cell> cells = new Dictionary<string, Cell>();
    Stack<Cell> stack = new Stack<Cell>();

    public Vector2Int size = Vector2Int.one;
    float partTreasure = 1f;
    float partEnemy = 1f;
    float partEmpty = 1f;

    [SerializeField] Transform roomContainer;
    [SerializeField] Transform doorContainer;

    [SerializeField] List<GameObject> lootOptions;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;

        Generate();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void Generate()
    {
        int c = 0;
        int r = 0;
        for (int y = 0; y <= size.y; y += 1)
        {
            for (int x = 0; x <= size.x; x += 1)
            {
                Cell cell = new Cell(new Vector2(x, y), Vector2.one, ref cells, r, c, (size.y - 1), (size.x - 1));
                c += 1;
            }
            c = 0;
            r += 1;
        }

        Debug.Log(LoadingCanvas.instance);
        LoadingCanvas.instance.showLoading = true;

        IEnumerator coroutine;
        coroutine = Dig();
        StartCoroutine(coroutine);
        generating = true;
    }

    private IEnumerator Dig()
    {
        string key = "c" + 5 + "r" + 5;
        Cell startCell = cells[key];
        stack.Clear();
        startCell.Visited = true;
        while ((startCell != null))
        {
            startCell = startCell.Dig(ref stack);
            if (startCell != null)
            {
                startCell.Visited = true;
            }
        }
        stack.Clear();

        MakeRooms();

        MakeDoors();

        generating = false;

        LoadingCanvas.instance.showLoading = false;

        yield return null;
    }

    void MakeDoors()
    {
        GameObject doorObject = Resources.Load<GameObject>("Door");

        for (int x = 0; x < size.x; x++)
        {
            for (int y = 0; y < size.y; y++)
            {
                Cell cell = cells["c" + x + "r" + y];
                //Debug.Log(cell.id + " [" + cell.NorthWall + ", " + cell.EastWall + ", " + cell.SouthWall + ", " + cell.WestWall + "]");
                if (cell.EastWall == true && Random.value < 0.1) cell.EastWall = false;
                if (cell.EastWall == false && x < size.x - 1)
                {
                    Instantiate(doorObject, new Vector3(x * 10 + 5, -4f, y * 10), Quaternion.Euler(0f, 90f, 0f), doorContainer);
                }

                if (cell.SouthWall == true && Random.value < 0.1) cell.SouthWall = false;
                if (cell.SouthWall == false && y < size.y - 1)
                {
                    Instantiate(doorObject, new Vector3(x * 10, -4f, y * 10 + 5), Quaternion.identity, doorContainer);
                }
            }
        }
    }

    void MakeRooms()
    {
        GameObject roomObject = Resources.Load<GameObject>("Room");

        Vector2 playerSpawn = Vector2.zero;
        playerSpawn.x = Random.Range(1, size.x - 1);
        playerSpawn.y = Random.Range(1, size.y - 1);

        Vector2 bossSpawn = playerSpawn;
        while (bossSpawn == playerSpawn)
        {
            bossSpawn.x = Random.Range(0, size.x);
            bossSpawn.y = Random.Range(0, size.y);
        }

        for (int x = 0; x <size.x; x++)
        {
            for (int y = 0; y < size.y; y++)
            {
                Cell cell = cells["c" + x + "r" + y];

                RoomController.RoomType roomType = RoomController.RoomType.NULL;

                if (x == playerSpawn.x && y == playerSpawn.y)
                {
                    roomType = RoomController.RoomType.playerSpawn;
                    playerSpawnPosition = new Vector3(x * 10, -3f, y * 10);
                }
                else if (x == bossSpawn.x && y == bossSpawn.y)
                {
                    roomType = RoomController.RoomType.floorBoss;
                }
                else
                {
                    float enemyOrTreasure = Random.Range(0, partTreasure + partEnemy + partEmpty);

                    if (enemyOrTreasure < partTreasure) roomType = RoomController.RoomType.treasure;
                    else if (enemyOrTreasure < partTreasure + partEnemy) roomType = RoomController.RoomType.enemy;
                    else roomType = RoomController.RoomType.empty;
                }

                RoomController roomController = Instantiate(roomObject, new Vector3(x * 10, 0f, y * 10), Quaternion.identity, roomContainer).GetComponent<RoomController>();
                roomController.roomType = roomType;
            }
        }


    }

    public GameObject GetTreasureObject()
    {
        if (lootOptions.Count == 0) return null;
        return lootOptions[Random.Range(0, lootOptions.Count)];
    }
}
