﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BloodScreenController : MonoBehaviour
{
    public static BloodScreenController instance;

    Image image;

    [SerializeField] float remainingTime = 0f;
    float maxTime = 1f;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        image = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update()
    {
        remainingTime -= Time.deltaTime;
        image.color = new Color(1f, 1f, 1f, remainingTime / maxTime);
    }

    public static void SetTime(float amount)
    {
        if (instance == null) return;
        instance.remainingTime = amount;
        instance.maxTime = amount;
    }
}
