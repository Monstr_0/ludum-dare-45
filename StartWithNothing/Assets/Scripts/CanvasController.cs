﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasController : MonoBehaviour
{
    public static Canvas mainCanvas;

    [SerializeField] CanvasGroup backLayer;
    [SerializeField] CanvasGroup middleLayer;
    [SerializeField] CanvasGroup frontLayer;
    [SerializeField] CanvasGroup screenEffectsLayer;

    public enum Layers
    {
        BACK,
        MIDDLE,
        FRONT,
        SCREEN_EFFECTS
    }

    private void Start()
    {
        mainCanvas = GetComponent<Canvas>();
    }

    public GameObject InstantiateOnLayer(GameObject obj, Layers layer)
    {
        Transform parentTransform;
        switch (layer)
        {
            case Layers.BACK:
                parentTransform = backLayer.transform; break;
            case Layers.FRONT:
                parentTransform = frontLayer.transform; break;
            case Layers.SCREEN_EFFECTS:
                parentTransform = screenEffectsLayer.transform; break;
            default:
                parentTransform = middleLayer.transform; break;
        }

        return Instantiate(obj, parentTransform);
    }

    public void SetLayerVisibility(Layers layer, bool visible, bool othersAreOpposite = false)
    {
        switch (layer)
        {
            case Layers.BACK:
                backLayer.alpha = visible ? 1 : 0;
                if (othersAreOpposite)
                {
                    frontLayer.alpha = visible ? 0 : 1;
                    middleLayer.alpha = visible ? 0 : 1;
                }
                break;
            case Layers.FRONT:
                frontLayer.alpha = visible ? 1 : 0;
                if (othersAreOpposite)
                {
                    backLayer.alpha = visible ? 0 : 1;
                    middleLayer.alpha = visible ? 0 : 1;
                }
                break;
            default:
                middleLayer.alpha = visible ? 1 : 0;
                if (othersAreOpposite)
                {
                    frontLayer.alpha = visible ? 0 : 1;
                    backLayer.alpha = visible ? 0 : 1;
                }
                break;
        }
    }

    public void ResetLayerVisibility()
    {
        frontLayer.alpha = 1f;
        middleLayer.alpha = 1f;
        backLayer.alpha = 1f;
        screenEffectsLayer.alpha = 1f;
    }
}
