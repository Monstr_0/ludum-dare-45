﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFollowObject : MonoBehaviour
{
    Canvas canvas;
    CanvasScaler canvasScaler;
    protected CanvasGroup canvasGroup;

    [Header("UI Follow World Object")]
    public GameObject worldObject;
    public Vector3 worldPosition = Vector3.zero;
    public Vector2 positionOffset = Vector2.zero;
    [SerializeField] protected bool containInsideScreen = true;
    [SerializeField] protected Vector2 addedBoundary = Vector2.zero;
    [SerializeField] protected Vector3 outsideScreenScale = Vector3.one;
    [SerializeField] protected Vector2 outsideScreenBoundary = Vector2.zero;
    [SerializeField] [Range(0, 1)] protected float outSideScreenFade = 1;
    [Space]
    public bool visible = true;
    [Space]
    [SerializeField] Vector2 debugPos;

    // Use this for initialization
    protected virtual void Start()
    {
        canvas = GetComponentInParent<Canvas>();
        canvasScaler = GetComponentInParent<CanvasScaler>();
        canvasGroup = GetComponent<CanvasGroup>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        if (worldObject != null)
        {
            worldPosition = worldObject.transform.position;
        }

        if (canvasScaler == null)
        {
            canvasScaler = GetComponentInParent<CanvasScaler>();
            return;
        }
        float uiScale = canvasScaler.referenceResolution.y / Camera.main.pixelHeight;

        //Vector3 screenPos = Camera.main.WorldToScreenPoint(worldPosition);
        Vector3 screenPos = WorldToScreenPointProjected(Camera.main, worldPosition);
        screenPos -= new Vector3(Camera.main.pixelWidth, Camera.main.pixelHeight, 0f) / 2;
        Vector2 pos = screenPos * uiScale;
        pos += positionOffset;
        debugPos = pos;
        bool outsideScreen = false;
        if (containInsideScreen == true)
        {
            if (canvas == null)
            {
                canvas = GetComponentInParent<Canvas>();
                Debug.LogWarning("Canvas was null!");
            }
            else
            {
                Vector2 canvasSize = new Vector2(canvasScaler.referenceResolution.x, canvasScaler.referenceResolution.y);
                Vector2 boundary = ((RectTransform)transform).sizeDelta + addedBoundary;
                Bounds screenBounds = new Bounds(Vector3.zero, new Vector3(canvasSize.x, canvasSize.y, 1f));

                if (!screenBounds.Contains(pos - boundary / 2) || !screenBounds.Contains(pos + boundary / 2))
                {
                    outsideScreen = true;
                    pos.x = Mathf.Clamp(pos.x, -canvasSize.x / 2 + boundary.x / 2 + outsideScreenBoundary.x / 2,
                        canvasSize.x / 2 - boundary.x / 2 - outsideScreenBoundary.x / 2);
                    pos.y = Mathf.Clamp(pos.y, -canvasSize.y / 2 + boundary.y / 2 + outsideScreenBoundary.y / 2,
                        canvasSize.y / 2 - boundary.y / 2 - outsideScreenBoundary.y / 2);
                }
            }
        }

        ((RectTransform)transform).anchoredPosition = pos;
        if (outsideScreenScale != Vector3.one)
        {
            if (!outsideScreen) transform.localScale = Vector3.one;
            else transform.localScale = outsideScreenScale;
        }

        if (canvasGroup)
        {
            float target;
            if (visible)
            {
                target = 1;
                if (outsideScreen) target = outSideScreenFade;
            }
            else target = 0;

            canvasGroup.alpha = Mathf.Lerp(canvasGroup.alpha, target, 0.5f);
        }
    }

    public static Vector2 WorldToScreenPointProjected(Camera camera, Vector3 worldPos)
    {
        Vector3 camNormal = camera.transform.forward;
        Vector3 vectorFromCam = worldPos - camera.transform.position;
        float camNormDot = Vector3.Dot(camNormal, vectorFromCam);
        if (camNormDot <= 0)
        {
            // we are behind the camera forward facing plane, project the position in front of the plane
            Vector3 proj = (camNormal * camNormDot * 1.01f);
            worldPos = camera.transform.position + (vectorFromCam - proj);
        }

        return RectTransformUtility.WorldToScreenPoint(camera, worldPos);
    }

    public Vector2 WorldToScreenPos(Vector3 worldPos)
    {
        CanvasScaler canvasScaler = GetComponentInParent<CanvasScaler>();
        float uiScale = canvasScaler.referenceResolution.y / Camera.main.pixelHeight;

        Vector3 screenPos = WorldToScreenPointProjected(Camera.main, worldPos);
        screenPos -= new Vector3(Camera.main.pixelWidth, Camera.main.pixelHeight, 0f) / 2;
        Vector2 pos = screenPos * uiScale;

        return pos;
    }
}
