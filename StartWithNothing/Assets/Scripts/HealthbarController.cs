﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthbarController : UIFollowObject
{
    [Header("HealthbarController")]
    [SerializeField] RectTransform barTransform;
    [Space]
    public float maxHealth = 100;
    public float currentHealth = 50;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        SetBarDisplay();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        SetBarDisplay();
    }

    void SetBarDisplay()
    {
        if (barTransform != null)
        {
            float fullWidth = ((RectTransform)barTransform.parent).sizeDelta.x;
            barTransform.sizeDelta = new Vector2(fullWidth * currentHealth / maxHealth, barTransform.sizeDelta.y);
        }
    }
}
