﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    [SerializeField] List<RoomController> connectedRooms;
    [Space]
    [SerializeField] MeshRenderer doorMeshRenderer;
    [SerializeField] GameObject lockedObject;

    int checkForRoomsTimer = 0;

    public bool isLocked { get; private set; }
    [SerializeField] bool setLocked = false;

    // Start is called before the first frame update
    void Start()
    {
        if (doorMeshRenderer != null) doorMeshRenderer.sharedMaterial = doorMeshRenderer.material;

        SetLocked(false);
    }

    private void Awake()
    {
        connectedRooms = new List<RoomController>(2);
        checkForRoomsTimer = 2;
    }

    // Update is called once per frame
    void Update()
    {
        if (checkForRoomsTimer > 0)
        {
            checkForRoomsTimer -= 1;
            if (checkForRoomsTimer == 0)
            {
                LayerMask layerMask = LayerMask.GetMask(new string[1] { "Room" });
                RaycastHit[] hitInfo = Physics.SphereCastAll(transform.position, 0.1f, Vector3.up, 0f);
                for (int i = 0; i < hitInfo.Length; i++)
                {
                    if (hitInfo[i].collider.CompareTag("Room"))
                    {
                        RoomController roomController = hitInfo[i].collider.GetComponent<RoomController>();
                        if (connectedRooms.Contains(roomController) == false)
                        {
                            connectedRooms.Add(roomController);
                        }
                    }
                }

                //Debug.Log("Checked for Rooms [" + layerMask.value.ToString() + "][" + hitInfo.Length + "]");
            }
        }
        
        float alphaValue = 0f;
        for (int i = 0; i < connectedRooms.Count; i++)
        {
            if (connectedRooms[i].isCurrent)
            {
                alphaValue = 1f;
                break;
            }
        }

        if (doorMeshRenderer.sharedMaterial.color.a != alphaValue)
        {
            doorMeshRenderer.sharedMaterial.color = new Color(
                doorMeshRenderer.sharedMaterial.color.r,
                doorMeshRenderer.sharedMaterial.color.g,
                doorMeshRenderer.sharedMaterial.color.b,
                Mathf.Lerp(doorMeshRenderer.sharedMaterial.color.a, alphaValue, 0.1f)
            );
        }

        if (setLocked != isLocked) SetLocked(setLocked);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Room"))
        {
            RoomController roomController = other.GetComponent<RoomController>();
            if (connectedRooms.Contains(roomController) == false)
            {
                connectedRooms.Add(roomController);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Room"))
        {
            RoomController roomController = other.GetComponent<RoomController>();
            connectedRooms.Remove(roomController);
        }
    }

    public void SetLocked(bool locked)
    {
        this.isLocked = locked;
        setLocked = locked;
        lockedObject.SetActive(locked);
    }
}
