﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : Actor
{
    PlayerInventory playerInventory;

    [SerializeField] float speed = 5f;
    [Space]
    [SerializeField] LayerMask defaultLayer;
    [SerializeField] LayerMask betweenDoorsLayer;
    [Space]
    public bool usingMouse = true;

    bool doorEntryInFront = false;

    [Space]
    [SerializeField] Weapon currentWeapon;

    Camera mainCamera;

    float scrollCooldown = 0f;

    [SerializeField] Transform weaponContainer;

    public bool hasWon = false;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        defaultLayer = gameObject.layer;

        mainCamera = Camera.main;

        currentWeapon = transform.GetComponentInChildren<Weapon>();

        CreateHealthbar();

        playerInventory = GetComponent<PlayerInventory>();
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        if (playerInventory == null) playerInventory = GetComponent<PlayerInventory>();

        if (!dead)
        {
            if (canControl)
            {
                Vector2 input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
                Vector2 inputConverted = ConvertInputUsingCamera(input);

                Vector3 movement = new Vector3(inputConverted.x, 0f, inputConverted.y);
                movement *= speed;
                rigidbody.velocity = new Vector3(movement.x, rigidbody.velocity.y, movement.z);

                Vector3 aimPos = transform.position + transform.forward;
                if (usingMouse)
                {
                    Ray mouseRay = CameraController.instance.camera.ScreenPointToRay(Input.mousePosition);
                    Plane plane = new Plane(Vector3.up, transform.position);
                    float enter;
                    if (plane.Raycast(mouseRay, out enter))
                    {
                        aimPos = mouseRay.GetPoint(enter);
                    }
                }

                transform.LookAt(aimPos);
                transform.eulerAngles = new Vector3(0f, transform.eulerAngles.y, 0f);
            }

            if (canAttack)
            {
                if (currentWeapon != null)
                {
                    if (Input.GetButton("Fire1")) currentWeapon.FireHold();
                    if (Input.GetButtonDown("Fire1")) currentWeapon.Fire();
                    if (Input.GetButtonUp("Fire1")) currentWeapon.FireRelease();
                }
            }

            if (scrollCooldown <= 0f)
            {
                if (Input.mouseScrollDelta.y != 0)
                {
                    int dir = (int)Mathf.Sign(Input.mouseScrollDelta.y);
                    string nextWeapon = playerInventory.GetNextInventory(dir);
                    if (nextWeapon != "")
                    {
                        for (int i = 0; i < weaponContainer.childCount; i++)
                        {
                            Destroy(weaponContainer.GetChild(i).gameObject);
                        }

                        GameObject weaponObject = Resources.Load<GameObject>(nextWeapon);
                        currentWeapon = Instantiate(weaponObject, weaponContainer).GetComponent<Weapon>();

                        Debug.Log("Switch Weapon to " + nextWeapon);
                    }

                    scrollCooldown = 0.05f;
                }
            }
            else scrollCooldown -= Time.deltaTime;

            DeathTextController.SetVisible(false);

            if (RoomController.current.roomType == RoomController.RoomType.playerSpawn)
            {
                InstructionsTextController.SetVisible(true);
            }
            else InstructionsTextController.SetVisible(false);
        }
        else
        {
            BloodScreenController.SetTime(1f);

            DeathTextController.SetVisible(true);

            InstructionsTextController.SetVisible(false);
        }

        if (hasWon && RoomController.current.roomType == RoomController.RoomType.floorBoss)
        {
            WinTextController.SetVisible(true);
        }
        else
        {
            WinTextController.SetVisible(false);
        }

        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    public override bool TakeDamage(int amount = 1)
    {
        BloodScreenController.SetTime(0.5f);

        return base.TakeDamage(amount);
    }

    protected override void Die()
    {
        base.Die();
    }

    public void RemoveWeapon(string weaponName = "")
    {
        if (weaponName != "")
        {
            if (currentWeapon != null && currentWeapon.gameObject.name.Contains(weaponName) == false) return;
        }

        currentWeapon = null;

        for (int i = 0; i < weaponContainer.childCount; i++)
        {
            Destroy(weaponContainer.GetChild(i).gameObject);
        }

        string nextWeapon = playerInventory.GetNextInventory(0);
        
        if (nextWeapon != "")
        {
            GameObject weaponObject = Resources.Load<GameObject>(nextWeapon);
            currentWeapon = Instantiate(weaponObject, weaponContainer).GetComponent<Weapon>();

            Debug.Log("Switch Weapon to " + nextWeapon);
        }
    }

    Vector2 ConvertInputUsingCamera(Vector2 input)
    {
        Vector3 forward3d = mainCamera.transform.forward;
        Vector2 forward2d = new Vector2(forward3d.x, forward3d.z);
        forward2d.Normalize();
        Vector2 forward2dPerp = Vector2.Perpendicular(forward2d);

        return forward2d * input.y + forward2dPerp * -input.x;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Door"))
        {
            if (other.GetComponent<DoorController>().isLocked == false)
            {
                gameObject.layer = 11;
            }

            Vector3 relativeToMePos = transform.position - other.transform.position;
            float angle = Vector3.Angle(other.transform.forward, relativeToMePos);

            doorEntryInFront = (angle < 90);
        }
        else if (other.CompareTag("Chest"))
        {
            ChestController chestController = other.GetComponent<ChestController>();
            if (chestController.isOpen == true) return;

            List<GameObject> contents = chestController.TakeContents();
            Debug.Log("Got " + contents.Count + " contents...");
            for (int i = 0; i < contents.Count; i++)
            {
                playerInventory.AddToInventory(contents[i].name);
                string nextWeapon = playerInventory.GetNextInventory(0);
                if (nextWeapon != "" && currentWeapon == null)
                {
                    GameObject weaponObject = Resources.Load<GameObject>(nextWeapon);
                    Instantiate(weaponObject, weaponContainer);
                    currentWeapon = transform.GetComponentInChildren<Weapon>();
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Door"))
        {
            gameObject.layer = 13;

            Vector3 relativeToMePos = transform.position - other.transform.position;
            float angle = Vector3.Angle(other.transform.forward, relativeToMePos);

            if (angle < 90 != doorEntryInFront)
            {
                SetNewCameraPos(other);

                RoomController.SetCurrentRoomClosestPosition(transform.position);
            }


        }
    }

    void SetNewCameraPos(Collider other) {

        bool shouldMove = Vector3.Distance(other.transform.position, CameraController.instance.targetPosition) >= 12;

        if (shouldMove)
        {
            Vector3 plane = other.transform.forward;
            Vector3 relativePos = CameraController.instance.targetPosition - RoomController.current.transform.position;
            Vector3 flippedPos = RoomController.current.transform.position - relativePos;

            bool alongX = (Mathf.Sign(plane.x) != Mathf.Sign(plane.z));
            if (alongX)
                CameraController.instance.targetPosition = new Vector3(flippedPos.x, CameraController.instance.targetPosition.y, CameraController.instance.targetPosition.z);
            else
                CameraController.instance.targetPosition = new Vector3(CameraController.instance.targetPosition.x, CameraController.instance.targetPosition.y, flippedPos.z);
        }
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Enemy"))
        {
            TakeDamage(5);
            Knockback(collision.GetContact(0).point, 3f);
        }
        else if (collision.collider.CompareTag("EnemyBoss"))
        {
            TakeDamage(10);
            Knockback(collision.GetContact(0).point, 2f);
            collision.collider.GetComponent<EnemyBossController>().HitPlayer();
        }
        else if (collision.collider.CompareTag("Room"))
        {
            canControl = true;
            canAttack = true;

            if (RoomController.current == null)
            {
                RoomController.SetCurrentRoomClosestPosition(transform.position);
            }
        }
        else if (collision.collider.CompareTag("Door"))
        {
            canControl = true;
            canAttack = true;
        }
    }
}
