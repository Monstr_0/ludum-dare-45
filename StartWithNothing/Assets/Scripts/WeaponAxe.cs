﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponAxe : Weapon
{
    Animator animator;

    List<Collider> colliderCollisons;
    List<Collider> collidersHit;

    bool checkCollider = false;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        animator = GetComponent<Animator>();
        colliderCollisons = new List<Collider>();
        collidersHit = new List<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        if (checkCollider)
        {
            int damageAmount = Mathf.Max(playerInventory.GetInventory("WeaponSword"), 1);
            for (int i = 0; i < colliderCollisons.Count; i++)
            {
                if (collidersHit.Contains(colliderCollisons[i])) continue;
                EnemyController enemyController = colliderCollisons[i].GetComponent<EnemyController>();
                BossSignController bossSignController = colliderCollisons[i].GetComponent<BossSignController>();
                if (enemyController != null)
                {
                    enemyController.TakeDamage(damageAmount);
                    enemyController.Knockback(playerController.transform.position, 3f);
                }
                else if (bossSignController != null)
                {
                    bossSignController.Activate(playerController.transform.position);
                    Debug.Log("Activate the Boss Sign!");
                }
                collidersHit.Add(colliderCollisons[i]);
            }
        }
    }

    public override void Fire()
    {
        base.Fire();

        animator.SetBool("Swing", true);
    }

    public override void FireHold()
    {
        base.FireHold();
    }

    public override void FireRelease()
    {
        base.FireRelease();

        animator.SetBool("Swing", false);
    }

    public void StartHitbox()
    {
        checkCollider = true;
        collidersHit.Clear();
    }

    public void StopHitbox()
    {
        checkCollider = false;
        collidersHit.Clear();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (colliderCollisons.Contains(other) == false) colliderCollisons.Add(other);
    }

    private void OnTriggerExit(Collider other)
    {
        colliderCollisons.Remove(other);
    }
}
