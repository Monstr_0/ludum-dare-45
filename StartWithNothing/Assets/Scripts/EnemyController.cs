﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : Actor
{
    [SerializeField] protected float speed = 2f;
    [Space]
    public float stunTime = 0f;

    protected Vector3 startingPosition;
    protected RoomController myRoom;
    public enum MovementType
    {
        Follow,
        StandStill, 
        MaintainDistance,
        Flee
    }

    protected static Transform playerTransform;

    public MovementType movementType = MovementType.Follow;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        startingPosition = transform.position;

        if (playerTransform == null)
        {
            GameObject obj = GameObject.FindGameObjectWithTag("Player");
            if (obj != null) playerTransform = obj.transform;
        }
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

        if (!dead)
        {
            if (stunTime > 0f)
            {
                canControl = false;
                canAttack = false;
                stunTime -= Time.deltaTime;

                if (stunTime <= 0f)
                {
                    canControl = true;
                    canAttack = true;
                }
            }

            if (canControl)
            {
                Vector3 targetPosition = transform.position;
                if (myRoom != null && myRoom.isCurrent)
                {
                    CreateHealthbar();

                    if (movementType == MovementType.Follow) targetPosition = playerTransform.position;
                    else if (movementType == MovementType.Flee)
                    {
                        targetPosition = playerTransform.position + (transform.position - playerTransform.position) * 2;
                    }
                    else if (movementType == MovementType.MaintainDistance)
                    {
                        Vector3 normalized = (transform.position - playerTransform.position).normalized;

                        targetPosition = playerTransform.position + normalized * 3;
                    }
                }
                else
                {
                    targetPosition = startingPosition;
                    health = maxHealth;
                    if (healthbarController != null)
                    {
                        Destroy(healthbarController.gameObject);
                        healthbarController = null;
                    }
                }

                Vector3 movement = (targetPosition - transform.position).normalized;
                movement.y = 0f;
                movement *= speed;
                rigidbody.velocity = new Vector3(movement.x, rigidbody.velocity.y, movement.z);
            }
        }
    }

    protected override void Die()
    {
        base.Die();

        if (healthbarController != null)
        {
            Destroy(healthbarController.gameObject);
        }
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Room"))
        {
            if (myRoom == null) myRoom = collision.collider.GetComponent<RoomController>();
        }
    }

    public override void Knockback(Vector3 fromPosition, float amount = 3)
    {
        base.Knockback(fromPosition, amount);

        stunTime = 0.25f;
    }
}
