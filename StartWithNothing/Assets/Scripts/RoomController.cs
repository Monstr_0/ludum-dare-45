﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomController : MonoBehaviour
{
    AudioSource audioSource;

    public static RoomController current { get; private set; }
    [SerializeField] bool currentOnStart = false;
    public bool isCurrent { get { return current == this; } }
    public static LayerMask layerMask { get; private set; }

    MeshRenderer meshRenderer;
    [SerializeField] MeshRenderer darknessMeshRenderer;

    [SerializeField] Transform objectContainer;

    public float alphaValue = 0f;

    bool playedSound = false;

    public enum RoomType
    {
        empty,
        playerSpawn,
        floorBoss,
        enemy,
        treasure,
        NULL
    }
    public RoomType roomType = RoomType.empty;

    // Start is called before the first frame update
    void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        meshRenderer.sharedMaterial = meshRenderer.material;
        if (darknessMeshRenderer) darknessMeshRenderer.sharedMaterial = darknessMeshRenderer.material;

        layerMask = gameObject.layer;

        if (currentOnStart) current = this;

        audioSource = GetComponent<AudioSource>();

        MakeContainedObjects();
    }

    // Update is called once per frame
    void Update()
    {
        alphaValue = (current == this) ? 1f : 0f;

        if (darknessMeshRenderer.sharedMaterial.color.a == alphaValue)
        {
            darknessMeshRenderer.sharedMaterial.color = new Color(0f, 0f, 0f, (meshRenderer.sharedMaterial.color.a <= 0.1f) ? 1f : 0f);
        }

        if (meshRenderer.sharedMaterial.color.a != alphaValue)
        {
            meshRenderer.sharedMaterial.color = new Color(
                meshRenderer.sharedMaterial.color.r, 
                meshRenderer.sharedMaterial.color.g, 
                meshRenderer.sharedMaterial.color.b, 
                Mathf.Lerp(meshRenderer.sharedMaterial.color.a, alphaValue, 0.05f)
            );
        }

        if (RoomController.current == this && !playedSound)
        {
            string clipName = "";
            if (roomType == RoomType.enemy) clipName = "SoundEnemy";
            else if (roomType == RoomType.treasure) clipName = "SoundTreasure";
            else if (roomType == RoomType.floorBoss) clipName = "SoundBoss";
            AudioClip clip = Resources.Load<AudioClip>(clipName);
            audioSource.clip = clip;
            audioSource.Play();
            playedSound = true;
        }
    }

    public static void SetCurrentRoomClosestPosition(Vector3 pos)
    {
        RoomController[] roomControllers = FindObjectsOfType<RoomController>();
        int closestIndex = -1;
        float closestDistance = -1f;
        for (int i = 0; i < roomControllers.Length; i++)
        {
            float dist = Vector3.Distance(roomControllers[i].transform.position, pos);
            if (closestIndex < 0 || dist < closestDistance)
            {
                closestIndex = i;
                closestDistance = dist;
            }
        }

        if (closestIndex < 0) return;

        current = roomControllers[closestIndex];
    }

    void MakeContainedObjects()
    {
        GameObject chestObject = Resources.Load<GameObject>("chest");
        GameObject enemyObject = Resources.Load<GameObject>("enemy");
        GameObject bossObject = Resources.Load<GameObject>("BossSign");
        GameObject playerObject = Resources.Load<GameObject>("Player");

        for (int i = 0; i < objectContainer.childCount; i++)
        {
            Destroy(objectContainer.GetChild(i).gameObject);
        }

        if (roomType == RoomType.playerSpawn)
        {
            GameObject obj = Instantiate(playerObject, transform.position, Quaternion.identity);
            current = this;
            Vector3[] positions = new Vector3[4]
            {
                transform.position + new Vector3(5f, 5f, 5f),
                transform.position + new Vector3(-5f, 5f, 5f),
                transform.position + new Vector3(5f, 5f, -5f),
                transform.position + new Vector3(-5f, 5f, -5f)
            };
            CameraController.instance.targetPosition = positions[Random.Range(0, positions.Length)];
            CameraController.instance.transform.position = CameraController.instance.targetPosition;
            CameraController.instance.lookAtTransform = obj.transform;
        }
        else if (roomType == RoomType.treasure)
        {
            ChestController chestController = Instantiate(chestObject, objectContainer).GetComponent<ChestController>();

            chestController.contents.Add(MapCreator.instance.GetTreasureObject());
            if (Random.value < 0.25f)
            {
                chestController.contents.Add(MapCreator.instance.GetTreasureObject());
                if (Random.value < 0.25f)
                {
                    chestController.contents.Add(MapCreator.instance.GetTreasureObject());
                }
            }

            chestController.UpdateMaterial();
        }
        else if (roomType == RoomType.enemy)
        {
            int numberOfEnemies = Random.Range(1, 4);
            if (numberOfEnemies == 1) Instantiate(enemyObject, objectContainer);
            else if (numberOfEnemies == 2)
            {
                Instantiate(enemyObject, transform.position + new Vector3(2.5f, 0f, 2.5f), Quaternion.identity, objectContainer);
                Instantiate(enemyObject, transform.position + new Vector3(-2.5f, 0f, -2.5f), Quaternion.identity, objectContainer);
            }
            else if (numberOfEnemies == 3)
            {
                Instantiate(enemyObject, transform.position + new Vector3(3f, 0f, 3f), Quaternion.identity, objectContainer);
                Instantiate(enemyObject, objectContainer);
                Instantiate(enemyObject, transform.position + new Vector3(-3f, 0f, -3f), Quaternion.identity, objectContainer);
            }
        }
        else if (roomType == RoomType.floorBoss)
        {
            Instantiate(bossObject, objectContainer);
        }
    }
}
