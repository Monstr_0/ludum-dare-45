﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Actor : MonoBehaviour
{
    AudioSource audioSource;

    protected new Rigidbody rigidbody;

    [SerializeField] protected bool canControl = true;
    [SerializeField] protected bool canAttack = true;
    [Space]
    public int maxHealth = 100;
    protected int health = 100;
    protected HealthbarController healthbarController;

    protected bool dead = false;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        rigidbody = GetComponent<Rigidbody>();

        health = maxHealth;

        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    protected virtual void Update()
    {
        
    }

    public virtual bool TakeDamage(int amount = 1)
    {
        health -= amount;

        CreateHealthbar();

        if (audioSource) audioSource.Play();

        if (health <= 0)
        {
            health = 0;

            healthbarController.maxHealth = maxHealth;
            healthbarController.currentHealth = health;

            Die();
            return true;
        }

        if (health < maxHealth)
        {
            healthbarController.maxHealth = maxHealth;
            healthbarController.currentHealth = health;
        }

        return false;
    }

    protected virtual void OnDestroy()
    {
        if (healthbarController != null)
        {
            Destroy(healthbarController.gameObject);
        }
    }

    protected virtual void Die()
    {
        gameObject.layer = 17;
        rigidbody.constraints = RigidbodyConstraints.None;
        rigidbody.AddExplosionForce(5f, transform.position + new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)), 2f, 0.5f, ForceMode.Impulse);
        rigidbody.AddTorque(new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f)) * 10f, ForceMode.Impulse);

        dead = true;
    }

    public void GainHealth(int amount = 1)
    {
        health += amount;
        if (health > maxHealth) health = maxHealth;

        if (healthbarController != null)
        {
            healthbarController.maxHealth = maxHealth;
            healthbarController.currentHealth = health;
        }
    }

    protected void CreateHealthbar()
    {
        if (healthbarController == null)
        {
            GameObject obj = CanvasController.mainCanvas.GetComponent<CanvasController>().InstantiateOnLayer(Resources.Load<GameObject>("Healthbar"), CanvasController.Layers.FRONT);
            healthbarController = obj.GetComponent<HealthbarController>();
            healthbarController.worldObject = gameObject;
            healthbarController.positionOffset = new Vector2(0f, -50f);

            healthbarController.maxHealth = maxHealth;
            healthbarController.currentHealth = health;
        }
    }

    public virtual void Knockback(Vector3 fromPosition, float amount = 3f)
    {
        Vector3 force = transform.position - fromPosition;
        force.Normalize();
        force *= 3f;
        force.y = 0f;
        rigidbody.AddForce(force, ForceMode.Impulse);
        rigidbody.velocity = new Vector3(rigidbody.velocity.x, 5f, rigidbody.velocity.z);
        canControl = false;
    }
}
