﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController instance { get; private set; }

    public new Camera camera { get; private set; }

    public Transform lookAtTransform;
    public bool look = true;

    public Vector3 targetPosition;

    // Start is called before the first frame update
    void Start()
    {
        camera = GetComponent<Camera>();

        targetPosition = transform.position;

        instance = this;
    }

    // Update is called once per frame
    void Update()
    {
        if (look && lookAtTransform != null) transform.LookAt(lookAtTransform);

        if (transform.position != targetPosition)
        {
            if (Vector3.Distance(transform.position, targetPosition) < 0.01f) transform.position = targetPosition;
            else transform.position = Vector3.Lerp(transform.position, targetPosition, 0.05f);
        }
    }
}
