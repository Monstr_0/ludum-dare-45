﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(BoxCollider))]
public class SnapToFloor : MonoBehaviour
{
    [SerializeField] LayerMask layerMask;
    [SerializeField] bool forceSnap = false;

    // Start is called before the first frame update
    void Start()
    {
        if (Application.isPlaying) Snap();
    }

    // Update is called once per frame
    void Update()
    {
        if (forceSnap)
        {
            Snap();
            forceSnap = false;
        }
    }

    bool Snap()
    {
        
        BoxCollider boxCollider = GetComponent<BoxCollider>();
        RaycastHit hitInfo;
        if (Physics.BoxCast(transform.position + boxCollider.center, boxCollider.size / 2, Vector3.down, out hitInfo, Quaternion.identity, 100f, layerMask))
        {
            if (hitInfo.collider.CompareTag("Room")) transform.Translate(Vector3.down * hitInfo.distance);
        }

        return false;
    }
}
