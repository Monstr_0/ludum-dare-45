﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUIController : MonoBehaviour
{
    [SerializeField] GameObject blockPrefab;

    public class Block
    {
        public Sprite sprite;
        public int amount;

        public Block(Sprite sprite, int amount)
        {
            this.sprite = sprite;
            this.amount = amount;
        }
    }

    public List<Block> blocks;

    // Start is called before the first frame update
    void Start()
    {
        blocks = new List<Block>();
    }

    public int FindBlockIndexWithSprite(Sprite sprite)
    {
        for (int i = 0; i < blocks.Count; i++)
        {
            if (blocks[i].sprite == sprite) return i;
        }

        return -1;
    }

    public void UpdateUI()
    {
        InventoryBlockController[] inventoryBlockControllers = GetComponentsInChildren<InventoryBlockController>();
        HashSet<int> indexTaken = new HashSet<int>();
        for (int i = 0; i < inventoryBlockControllers.Length; i++)
        {
            int index = inventoryBlockControllers[i].index;
            if (index < 0) continue;
            else if (index >= blocks.Count)
            {
                Destroy(inventoryBlockControllers[i].gameObject);
                continue;
            }

            if (index == 0) inventoryBlockControllers[i].holding = true;
            else inventoryBlockControllers[i].holding = false;

            inventoryBlockControllers[i].sprite = blocks[index].sprite;
            inventoryBlockControllers[i].amount = blocks[index].amount;

            indexTaken.Add(index);
        }
        for (int i = 0; i < blocks.Count; i++)
        {
            if (indexTaken.Contains(i)) continue;
            else
            {
                InventoryBlockController inventoryBlockController = Instantiate(blockPrefab, transform).GetComponent<InventoryBlockController>();
                inventoryBlockController.index = i;
                inventoryBlockController.sprite = blocks[i].sprite;
                inventoryBlockController.amount = blocks[i].amount;
                inventoryBlockController.holding = (i == 0);
                indexTaken.Add(i);
            }
        }

        indexTaken.Clear();
    }
}
