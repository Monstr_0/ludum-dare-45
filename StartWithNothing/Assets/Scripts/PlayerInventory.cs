﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    Dictionary<string, int> inventory;
    [SerializeField] List<string> inventoryOrder;

    InventoryUIController inventoryUIController;

    int currentIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        inventory = new Dictionary<string, int>();
        inventoryUIController = FindObjectOfType<InventoryUIController>();
        inventoryOrder = new List<string>();
    }

    // Update is called once per frame
    void Update()
    {
        if (inventoryUIController == null) inventoryUIController = FindObjectOfType<InventoryUIController>();
    }

    public void AddToInventory(string objName)
    {
        
        int current = 0;
        inventory.TryGetValue(objName, out current);
        Debug.Log("Adding to inventory: " + objName + " [" + (current <= 0).ToString() + ", " + (inventoryOrder.Contains(objName) == false).ToString() + "]");
        if (current <= 0 && inventoryOrder.Contains(objName) == false) inventoryOrder.Add(objName);

        inventory[objName] = Mathf.Max(current + 1, 1);

        UpdateUI();
    }

    public bool RemoveFromInventory(string objName, int amount = 100)
    {
        int current = 0;
        inventory.TryGetValue(objName, out current);
        if (current == 0 && inventoryOrder.Contains(objName) == false) inventoryOrder.Add(objName);
        inventory[objName] = current - amount;

        UpdateUI();

        if (inventory[objName] <= 0)
        {
            Debug.Log(objName + ": EMPTY");
            inventoryOrder.Remove(objName);
            return true;
        }

        return false;
    }

    public int GetInventory(string objName)
    {
        int current = 0;
        inventory.TryGetValue(objName, out current);
        return current;
    }

    public string GetNextInventory(int dir = 1)
    {
        currentIndex += dir;
        if (currentIndex < 0) currentIndex = inventoryOrder.Count - 1;
        else if (currentIndex >= inventoryOrder.Count) currentIndex = 0;

        UpdateUI();

        if (currentIndex < 0) currentIndex = inventoryOrder.Count - 1;
        if (currentIndex >= inventoryOrder.Count) currentIndex = 0;

        if (currentIndex >= 0 && currentIndex < inventoryOrder.Count) return inventoryOrder[currentIndex];
        else return "";
    }

    void UpdateUI()
    {
        if (inventoryUIController == null) return;

        inventoryUIController.blocks = new List<InventoryUIController.Block>();

        if (currentIndex < 0) currentIndex = inventoryOrder.Count - 1;
        if (currentIndex >= inventoryOrder.Count) currentIndex = 0;

        for (int i = currentIndex; i < inventoryOrder.Count; i++)
        {
            string name = inventoryOrder[i];
            Sprite sprite = Resources.Load<Sprite>("Icon" + name);
            InventoryUIController.Block newBlock = new InventoryUIController.Block(sprite, inventory[name]);
            inventoryUIController.blocks.Add(newBlock);
        }

        for (int i = 0; i < currentIndex; i++)
        {
            string name = inventoryOrder[i];
            Sprite sprite = Resources.Load<Sprite>("Icon" + name);
            InventoryUIController.Block newBlock = new InventoryUIController.Block(sprite, inventory[name]);
            inventoryUIController.blocks.Add(newBlock);
        }

        inventoryUIController.UpdateUI();
    }
}
