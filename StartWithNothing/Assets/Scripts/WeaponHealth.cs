﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponHealth : Weapon
{
    Animator animator;

    float waitTimer = 0.5f;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (waitTimer > 0) waitTimer -= Time.deltaTime;
    }

    public override void Fire()
    {
        if (waitTimer <= 0)
        {
            base.Fire();

            animator.ResetTrigger("Drink");
            animator.SetTrigger("Drink");

            waitTimer = 0.5f;
        }
    }

    public void ConsumeFlask()
    {
        if (waitTimer <= 0f)
        {
            Debug.Log("Consume Flask");
            playerController.GainHealth(25);
            waitTimer = 0.5f;
            bool empty = playerInventory.RemoveFromInventory("WeaponHealth", 1);
            if (empty)
            {
                playerController.RemoveWeapon("WeaponHealth");
            }
        }
    }
}
