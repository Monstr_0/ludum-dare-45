﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossSignController : MonoBehaviour
{
    new Rigidbody rigidbody;

    float deathTimer = 0f;

    bool canActivate = false;

    [SerializeField] GameObject bossPrefab;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0f, Time.deltaTime * 8, 0f);

        if (rigidbody.isKinematic == false)
        {
            if (deathTimer >= 0f)
            {
                deathTimer -= Time.deltaTime;
            }
            else
            {
                Debug.Log("Summon the Boss");

                if (bossPrefab != null) Instantiate(bossPrefab, transform.parent);

                Destroy(gameObject);
            }
        }

        if (canActivate)
        {
            if (Input.GetButtonDown("Fire2") || Input.GetButtonDown("Jump"))
            {
                canActivate = false;
                Activate(FindObjectOfType<PlayerController>().transform.position);
            }
        }
    }

    public void Activate(Vector3 position)
    {
        rigidbody.isKinematic = false;
        rigidbody.constraints = RigidbodyConstraints.None;
        deathTimer = 2f;
        Vector3 force = (transform.position - position).normalized;
        rigidbody.AddForceAtPosition(force, position + Vector3.up, ForceMode.Impulse);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            canActivate = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            canActivate = false;
        }
    }
}
