﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestController : MonoBehaviour
{
    Animator animator;

    [SerializeField] Material bronzeMaterial;
    [SerializeField] Material silverMaterial;
    [SerializeField] Material goldMaterial;
    [Space]
    public List<GameObject> contents;


    public bool isOpen { 
        get {
            if (animator == null) return false;
            else return animator.GetBool("Open");
        } 
    }

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetOpen(bool open)
    {
        if (animator) animator.SetBool("Open", open);
    }

    public List<GameObject> TakeContents()
    {
        if (isOpen == false)
        {
            SetOpen(true);
            return contents;
        }

        return null;
    }

    public void UpdateMaterial()
    {
        if (contents.Count <= 1)
        {
            SetMaterial(bronzeMaterial);
            return;
        }
        else if (contents.Count <= 2)
        {
            SetMaterial(silverMaterial);
            return;
        }
        else
        {
            SetMaterial(goldMaterial);
            return;
        }
    }

    void SetMaterial(Material material)
    {
        MeshRenderer[] meshRenderers = GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < meshRenderers.Length; i++)
        {
            meshRenderers[i].sharedMaterial = material;
        }
    }
}
