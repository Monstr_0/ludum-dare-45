﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class InventoryBlockController : MonoBehaviour
{
    RectTransform rectTransform;

    public static float yValue = 0f;

    Image image;
    CanvasGroup canvasGroup;
    [SerializeField] Image imageIcon;
    [SerializeField] TextMeshProUGUI textMeshPro;
    [Space]
    public int index = 0;
    public Sprite sprite;
    public int amount = 0;
    public bool holding = false;

    private void Start()
    {
        image = GetComponent<Image>();
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
    }

    // Update is called once per frame
    void Update()
    {
        imageIcon.sprite = sprite;
        textMeshPro.text = Mathf.Clamp(amount, 0, 99).ToString();

        if (holding) image.color = new Color(1f, 1f, 1f, 0.25f);
        else image.color = Color.clear;

        if (rectTransform == null) rectTransform = GetComponent<RectTransform>();
        rectTransform.anchoredPosition = new Vector2(100f * index, yValue);

        if (canvasGroup == null) canvasGroup = GetComponent<CanvasGroup>();
        if (sprite == null || amount <= 0) canvasGroup.alpha = 0;
        else canvasGroup.alpha = 1;
    }
}
