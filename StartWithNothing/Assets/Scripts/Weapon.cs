﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    protected PlayerController playerController;
    protected PlayerInventory playerInventory;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        playerInventory = GetComponentInParent<PlayerInventory>();
        playerController = GetComponentInParent<PlayerController>();
    }

    public virtual void Fire()
    {

    }

    public virtual void FireHold()
    {

    }

    public virtual void FireRelease()
    {

    }
}
