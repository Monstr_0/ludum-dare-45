﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LoadingCanvas : MonoBehaviour
{
    public static LoadingCanvas instance;

    public bool showLoading = false;
    public string text = "Loading...";

    CanvasGroup canvasGroup;

    [SerializeField] Image fluctuatingForeground;
    [SerializeField] TextMeshProUGUI textMeshPro;
    int fluctuateDir = 1;

    // Start is called before the first frame update
    void Start()
    {
        if (instance != null) Destroy(gameObject);
        else instance = this;

        DontDestroyOnLoad(gameObject);

        canvasGroup = GetComponent<CanvasGroup>();
    }

    // Update is called once per frame
    void Update()
    {
        if (showLoading)
        {
            canvasGroup.alpha = 1;
            if (fluctuatingForeground != null)
            {
                float alpha = fluctuatingForeground.color.a;
                if (fluctuateDir > 0)
                {
                    alpha += Time.deltaTime;
                    if (alpha >= 1) fluctuateDir = -1;
                }
                else if (fluctuateDir < 0)
                {
                    alpha -= Time.deltaTime;
                    if (alpha <= 0) fluctuateDir = 1;
                }
                fluctuatingForeground.color = new Color(fluctuatingForeground.color.r, fluctuatingForeground.color.g, fluctuatingForeground.color.b, alpha);
            }
        }
        else
        {
            canvasGroup.alpha = 0;
        }

        textMeshPro.text = text;
    }
}
