﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponBow : Weapon
{
    Animator animator;

    List<Collider> colliderCollisons;
    List<Collider> collidersHit;

    float pullPower = 0f;

    bool checkCollider = false;

    [SerializeField] GameObject arrowPrefab;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();

        animator = GetComponent<Animator>();
        colliderCollisons = new List<Collider>();
        collidersHit = new List<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void Fire()
    {
        base.Fire();

        pullPower = 0f;
        animator.ResetTrigger("Fire");
    }

    public override void FireHold()
    {
        base.FireHold();

        pullPower = Mathf.Clamp(pullPower + Time.deltaTime, 0f, 1f);
        animator.SetFloat("PullPower", pullPower);
    }

    public override void FireRelease()
    {
        base.FireRelease();

        animator.SetTrigger("Fire");

        FireArrow(pullPower);

        pullPower = 0f;
        animator.SetFloat("PullPower", 0f);

        
    }

    void FireArrow(float power)
    {
        if (arrowPrefab)
        {
            BowArrowController bowArrowController = Instantiate(arrowPrefab, playerController.transform.position + playerController.transform.forward * 2, playerController.transform.rotation).GetComponent<BowArrowController>();
            bowArrowController.GetComponent<Rigidbody>().velocity = playerController.transform.forward * (5f + 15f * power) + Vector3.up * (1f + 1f * power);
            bowArrowController.damage = Mathf.Max(playerInventory.GetInventory("WeaponSword"), 1);
        }
    }
}
